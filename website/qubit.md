---
layout: default
title: Qu'est-ce qu'un Qubit ?
custom_css: article
js: article
third_party_js: mathjax
---

<div class="article">
# {{ page.title }}
{:.no_toc .title}

<section>
# Ordinateur classique

Sur un ordinateur classique, tout fonctionne à l'aide d'unités de base appelés **bits**.
Un bit peut prendre 2 valeurs : 0 ou 1. Ce système binaire permet de représenter des nombres,
d'encoder des textes, d'effectuer des calculs ...

Malgré la puissance de calcul inimaginable de nos ordinateurs aujourd'hui, pouvant aller jusqu'à
3 milliard d'opérations par seconde pour nos ordinateurs grand public jusqu'à 16,32 millions de milliards d’opérations pour les supercalculateurs d'IBM, certains problèmes restent insolubles, car algorithmiquement trop long à exécuter sur un ordinateur.  
Par exemple, la factorisation en nombre premier est un problème complexe à résoudre (il faudrait des siècles de calcul). C'est notamment sur cette difficulté qu'est basée l'entiereté du chiffrement des systèmes bancaires.
</section>

<section>
# Ordinateur quantique - Qubit

Deuis les années 1980, certains chercheurs en mécanique quantique sont parvenus à théoriser un concept d'ordinateur quantique, qui exploite les propriétés de superposition et d'intrication de la mécanique quantique. Un tel ordinateur pourrait permettre de résoudre certains problèmes beaucoup plus rapidement.

Un **quantum bit**, ou **qubit**, est un bit qui exploite les propriétés de la mécanique quantique. Comment cela est-il possible ?

<section>
## Photon et polarisation

Pour une onde électromagnétique, la **polarisation** est la direction de vibration du champ électrique. Les **filtres polariseurs** permettent de laisser passer les ondes qui vibrent selon une certaine direction, déterminée par l'orientation du polariseur. Au niveau quantique, les photons présentent également cette propriété de polarisation.

<div class="figure">
![Filtre polariseur](assets/img/polarisation.png)
_Polarisation d'une onde et polarisation d'un photon. Le deuxième polariseur est perpendicualire au premier et absorbe donc le faisceau issu du premier polariseur._
</div>

Un photon polarisé _parallèlement_ à l'axe du polariseur sera transmis, tandis qu'un photon polarisé _perpendiculairement_ à l'axe du polariseur sera absorbé.

Mais que se passe-t-il si le photon n'est polarisé ni parallèlement, ni perpendiculairement ?

<div class=figure>
![Photon polarisé](assets/img/polarisation_photon.png)
_Photon (en rouge) polarisé à 45° par rapport à l'axe du polariseur_
</div>

Dans ce cas le photon a une probabilité de 0.5 d'être absorbé, et 0.5 d'être transmis. De plus, si le photon est transmis, il devient polarisé selon l'axe du polariseur (phénomène de réduction du paquet d'onde). Il s'agit donc d'un **état superposé** (voir [Introduction à la physique quantique](intro_quantique.html)).

On peut donc utiliser le photon polarisé pour implémenter l'équivalent quantique d'un bit, le **qubit**. L'état $\ket{\psi}$ de polarisation du photon peut donc être :
- Vertical : noté $\ket{0}$
- Horizontal : noté $\ket{1}$  
Le choix des directions de mesures est arbitraire, on aurait pu choisir les deux directions diagonales par exemple.

Outre ces états de base, le photon polarisé peut être dans une **superposition d'état**. Par exemple, pour une polarisation rectiligne de direction $\alpha$ ($\alpha = 45°$ dans la figure ci dessus), on a $\ket{\psi} = cos(\alpha)\ket{0} + sin(\alpha)\ket{1}$.
Il est également possible de construire des qubits en utilisant le spin 1/2 de l'électron.
</section>

<section>
## Qubit et premier postulat : la sphère de Bloch
Ainsi, contrairement à son homologue classique, le qubit peut valoir 0, 1 et toute combinaison linéaire de ces deux valeurs. L'espace des états $\mathcal{E}$ pour un qubit est donc de dimension 2 et les vecteurs de la base sont $\ket{0}$ et $\ket{1}$. Il s'écrit donc de la forme :

<div class="equation">
$$ \ket{\psi} = a \ket{0} + b \ket{1} $$  
avec $$ a \in \mathbb{C}, b \in \mathbb{C}, |a|^2 + |b|^2 = 1 $$
</div>

La probabilité d'obtenir 0 après la mesure est donc $|a|^2$ et celle d'obtenir 1 est $|b|^2$ (comme pour les fentes de Young, voir la page [Introduction à la physique quantique](intro_quantique.html)).
Le vecteur d’état est défini à un facteur de phase arbitraire près, $e^{i\alpha}a\ket{0}+e^{i\alpha}b\ket{1}$ représente le même état $\ket{\psi}$.

On peut démontrer que l'espace des états pour un qubit est analogue (homéomorphe) à une **sphère** de rayon 1, appelée **sphère de Bloch**.
Un point de coordonnées sphérique $\theta$ et $\phi$ sur cette sphère correspond à l'état :

<div class=equation>
$$ \ket{\psi} = cos(\frac{\theta}{2})\ket{0} + e^{i\phi}sin(\frac{\theta}{2})\ket{1} $$
</div>

Ce point est représenté par le **vecteur de Bloch**, noté $\vec{B_{\ket{\psi}}}$.

<div class=figure>
![Sphere de bloch](assets/img/sphere_bloch.png)
_La sphère de Bloch_
</div>

Cette sphère est très utilisée car elle est pratique pour visualiser l'état dans lequel est un qubit. Les états correspondants aux points extrèmes de la sphère de Bloch portent des noms particuliers, car sont beaucoup utilisés. Ces états, notés sur la figure ci-dessous, ont la particularité d'avoir autant de chance de donner 0 que 1 après la mesure (probabilité de 0.5 pour chaque état).

<div class=figure>
![Sphere de bloch notations](assets/img/sphere_bloch_notations.png)
_Etats aux extrémités de la sphère de Bloch_
</div>

</section>

Dans la [page suivante](portes_un_qubit.html), nous verrons comment l'action de **porte quantique** peut faire évoluer l'état d'un qubit.
</section>

</div>

{% include toc.html %}
