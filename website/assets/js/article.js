/**
  * @file Inclu dans tous les articles. Ce script regarde si des balises "h1" ou "h2"
  * sont présentes sur l'écran. Si oui, il ajoute la classe CSS "active" au lien correspondant
  * dans la table des matières. Cela permet de colorer la section actuelle de l'article dans la
  * TOC.
  */
window.addEventListener('DOMContentLoaded', () => {

  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      const id = entry.target.children[0].getAttribute('id');
			var navLink = document.querySelector(`nav li a[href="#${id}"]`)
			if (navLink != null) {
				if (entry.intersectionRatio > 0) {
					navLink.parentElement.classList.add('active');
	      } else {
	       	navLink.parentElement.classList.remove('active');
	      }
			}
    });
  });

  // Track all sections that have an `id` applied
  document.querySelectorAll('section').forEach((section) => {
    observer.observe(section);
  });

});

// // pour ouvrir la page dans une nouvelle fenêtre
// $(document).ready(function() {
//   $('a[href^="http"]').each(function() {
//     $(this).attr('target', '_blank');
//   });
// });
