import validateArgs from './utils/validateArgs.js';
import typecheck from './utils/typecheck.js';

export default class Complex {
  constructor(real, imaginary) {
    validateArgs(arguments, 1, 2, 'Must supply a real, and optionally an imaginary, argument to Complex()');
    imaginary = imaginary || 0;
    this.real = real;
    this.imaginary = imaginary;
  }

  add(other) {
    validateArgs(arguments, 1, 1, 'Must supply 1 parameter to add()');
    if (typecheck.isNumber(other)) {
      return new Complex(this.real + other, this.imaginary);
    }
    return new Complex(this.real + other.real, this.imaginary + other.imaginary);
  }

  multiply(other) {
    validateArgs(arguments, 1, 1, 'Must supply 1 parameter to multiply()');
    if (typecheck.isNumber(other)) {
      return new Complex(this.real * other, this.imaginary * other);
    }
    return new Complex(
      this.real * other.real - this.imaginary * other.imaginary,
      this.real * other.imaginary + this.imaginary * other.real
    );
  }

  conjugate() {
    return new Complex(this.real, -this.imaginary);
  }

  toString() {
    if (this.imaginary === 0) return `${this.real}`;
    let imaginaryString;
    if (this.imaginary === 1) {
      imaginaryString = 'i';
    } else if (this.imaginary === -1) {
      imaginaryString = '-i';
    } else {
      imaginaryString = `${this.imaginary}i`;
    }
    if (this.real === 0) return imaginaryString;
    const sign = (this.imaginary < 0) ? '' : '+';
    return this.real + sign + imaginaryString;
  }

  // added function
  toStringMathjax() {
    let realString, imaginaryString;
    if (Math.abs(this.real) == 0.7071)
      realString = (this.real > 0) ? "\\frac{1}{\\sqrt(2)}" : "-\\frac{1}{\\sqrt(2)}";
    else
      realString = this.real.toString();
    if (this.imaginary === 0) return `${this.real}`;
    if (this.imaginary === 1) {
      imaginaryString = 'i';
    } else if (this.imaginary === -1) {
      imaginaryString = '-i';
    } else if (Math.abs(this.imaginary) == 0.7071) {
      imaginaryString = (this.imaginary > 0) ? "\\frac{1}{\\sqrt(2)}i" : "-\\frac{1}{\\sqrt(2)}i";
    } else {
      imaginaryString = `${this.imaginary}i`;
    }
    if (this.real === 0) return imaginaryString;
    const sign = (this.imaginary < 0) ? '' : '+';
    return realString + sign + imaginaryString;
  }

  inspect() {
    return this.toString();
  }

  format(options) {
    let realValue = this.real;
    let imaginaryValue = this.imaginary;
    if (options && options.decimalPlaces != null) {
      const roundingMagnitude = Math.pow(10, options.decimalPlaces);
      realValue = Math.round(realValue * roundingMagnitude) / roundingMagnitude;
      imaginaryValue = Math.round(imaginaryValue * roundingMagnitude) / roundingMagnitude;
    }
    const objectToFormat = new Complex(realValue, imaginaryValue);
    if (options && options.mathjax == true)
      return objectToFormat.toStringMathjax();
    else
      return objectToFormat.toString();
  }

  negate() {
    return new Complex(-this.real, -this.imaginary);
  }

  magnitude() {
    return Math.sqrt(this.real * this.real + this.imaginary * this.imaginary);
  }

  phase() {
    // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/atan2
    return Math.atan2(this.imaginary, this.real);
  }


  subtract(other) {
    validateArgs(arguments, 1, 1, 'Must supply 1 parameter to subtract()');
    if (typecheck.isNumber(other)) {
      return new Complex(this.real - other, this.imaginary);
    }
    return new Complex(this.real - other.real, this.imaginary - other.imaginary);
  }

  eql(other) {
    if (!(other instanceof Complex)) return false;
    return this.real === other.real && this.imaginary === other.imaginary;
  }

  equal(other) {
    return this.eql(other);
  }

  equals(other) {
    return this.eql(other);
  }

  closeTo(other) {
    return Math.abs(this.real - other.real) < 0.0001 && Math.abs(this.imaginary - other.imaginary) < 0.0001;
  }


}
