# Demonstrateur algorithmes quantiques

Développement d'un site web pédagogique sur le thème des algorithmes quantiques.

Fonctionne avec le générateur de site statique jekyll : https://jekyllrb.com/.

Une documentation du fonctionnement du site est disponible dans _/doc/build/doc_tx.pdf_.

### Ouvrir en mode développement

Démarrer le site avec le serveur jekyll.

> $ bundle install  
  $ bundle exec jekyll serve

Il suffit ensuite d'aller à l'URL [http://127.0.0.1:4000]. Jekyll recompile le site automatiquement à chaque changement dans un des fichiers.

### Déployer le site

> $ bundle install  
	$ bundle exec jeyll build

Copier le contenu du dossier *_site* sur un serveur web.
