\definecolor{lightgray}{rgb}{0.93, 0.93, 0.93}

\nnsection{Introduction}

Ce document présente la documentation du site web développé dans le cadre d'une TX. L'ensemble des développements y sont expliqués, ainsi que la structure du site et les différents logiciels et fichiers utilisés.

\section{Jekyll}

La totalité du site web a été réalisée avec l'utilitaire \textbf{Jekyll} \cite{jekyll}. Il s'agit d'un générateur de site web statique, c'est à dire un site web dont le contenu ne varie pas selon les utilisateurs.

\subsection{Pourquoi Jekyll ?}

Un logiciel comme Jekyll permet de faciliter le développement d'un site web en étendant les fonctionnalités proposés par les langages HTML et CSS, qui deviennent vite limités lorsque le site comporte de nombreuses pages. Les avantages d'un tel générateur sont nombreux :
\begin{itemize}
	\item \underline{Factorisation de code} : système d'inclusion de fichiers HTML (pratique pour les barres de navigation, les \textit{footers} ...) et de \textit{template}
	\item \underline{Simplification de l'écriture HTML} : possibilité d'écrire des pages en syntaxe \textbf{Markdown} très simple à utiliser
	\item \underline{Fonctionnalités uniques :} génération automatique de table des matières, définition de données et de variables au format YAML pour générer des pages ...
\end{itemize}
Ces fonctionnalités permettent l'automatisation de la génération de pages web sans avoir à installer de services PHP. La génération produit uniquement des fichiers HTML, CSS et JS, il est donc très simple de déployer le site en copiant les fichiers à l'intérieur de n'importe quel serveur HTTP.

D'autres générateurs de ce type existent comme Hugo, mais Jekyll nous a paru plus intéressant pour ce projet.

\newpage

\subsection{Installation}

Le site de Jekyll propose une documentation fournie sur l'installation du logiciel, selon l'OS utilisé : \url{https://jekyllrb.com/docs/installation/}. Il est tout de même conseillé d'utiliser un système d'exploitation Linux car l'installation y est beaucoup plus facile et rapide. Les développements ont été effectués sur Ubuntu 20.10.

Après avoir installé Jekyll, ouvrez une console dans le répertoire du site, installez les dépendances et ouvrez un serveur Jekyll :
\begin{minted}[frame=lines,fontsize=\footnotesize,bgcolor=lightgray]{bash}
$ bundle install
$ bundle exec jekyll serv
\end{minted}

Le site est alors accessible à l'adresse localhost:4000. Le serveur Jekyll met à jours le site généré à chaque changement dans le code source.

\subsection{Déploiement}

Pour déployer le site, il suffit de copier le contenu du dossier \texttt{\_site} dans un serveur HTTP accessible par internet.
Si le dossier \texttt{\_site} n'existe pas, il faut le générer :
\begin{minted}[frame=lines,fontsize=\footnotesize,bgcolor=lightgray]{bash}
$ bundle exec jekyll build
\end{minted}
Il est également possible de minimiser les fichiers HTML, CSS, et JS pour gagner de la place, supprimer les commentaires, et rendre les requêtes vers le serveur plus rapides. Il suffit de lancer le script \texttt{minify} situé dans le dossier \texttt{minify-tool} :
\begin{minted}[frame=lines,fontsize=\footnotesize,bgcolor=lightgray]{bash}
$ ./minify ../website/_site
\end{minted}
Il faut avant cela installer les commandes \texttt{html-mignifier}, \texttt{csso} et \texttt{uglify-js} comme indiqué dans \texttt{minify-tool/README.md}.

\newpage

\subsection{Utilisation}

L'utilisation de Jekyll est très simple et assez intuitive. Le site propose un tutoriel plutôt rapide pour prendre en main l'utilitaire, disponible sur la page \url{https://jekyllrb.com/docs/step-by-step/01-setup/}. Il est conseillé d'y jeter un coup d'oeil.

Il faut retenir que Jekyll utilise 2 syntaxes très simples :
\begin{itemize}
	\item \underline{La syntaxe \texttt{Front Matter}} : Elle permet de définir des variables dans sa page web. On la retrouve au début des documents HTML dans un encadré comme celui-ci :
\begin{minted}[bgcolor=lightgray]{yaml}
---

---
\end{minted}
	On peut alors définir dans cet encadré le template qui sera utilisé, le nom de la page, ou définir ses propres variables.
	\item \underline{La syntaxe \texttt{Liquid}} : Elle fournit des structures \texttt{if/else} et des boucles \texttt{for} afin de pouvoir programmer la génération de son site, en utilisant parfois les variables définies dans le \texttt{Front Matter}
\end{itemize}
\newpage

\section{Structure du site}

\subsection{Arborescence}
Le site est organisé selon une architecture de dossier précise. On retrouve l'arborescence classique des projets sous Jekyll (voir Fig. \ref{arbo_site}). Les dossiers préfixés par le symbole '\_' son spécifiques à Jekyll.
\begin{figure}[h]
\begin{minted}[frame=lines,fontsize=\footnotesize,bgcolor=lightgray]{bash}
- assets
  |- css
     | .css files
  |- img
     | .png, .svg, .jpg files
  |- js
     |- thirdparty
        | .js files
     | .js files
- _data
  | navigation.yml
- _includes
  | navbar.html
  | toc.html
- _layouts
  | default.html
html pages files (*.html, *.md)
_config.yml
Gemfile
Gemfile.lock
\end{minted}
\caption{Arborescence de l'environnement de développement du site}
\label{arbo_site}
\end{figure}
\begin{itemize}
	\item \underline{Racine du site} : Dans la racine sont accessible tous les fichiers HTML ou Markdown qui représentent les différentes pages du site, ainsi que le fichier \texttt{\_config.yml}, dans lequel sont définis divers options pour configurer Jekyll. Les fichiers \texttt{Gemfile} et \texttt{Gemfile.lock} permettent de gérer les dépendances du projet. C'est dans ces fichiers que la commande \texttt{bundle install} regarde lors de l'installation du site.
	\item \underline{Dossier \texttt{assets}} : on y retrouve les feuilles de style css ainsi que les scripts js utilisés dans le site. Ces fichiers portent le même nom que les pages du site dans lesquels ils sont utilisés. Un dossier \texttt{img} contient toutes les images utilisées sur le site.
	\item \underline{Dossier \texttt{\_data}} :  dossier spécifique de Jekyll dans lequel on peut définir des variables réutilisables dans les pages du site, au format YAML. Dans notre cas, un seul fichier \texttt{navigation.yml} est accessible dans ce dossier. Il définit la structure arborescente de la barre de navigation, et est utilisé dans \texttt{\_includes/navbar.html} pour générer automatiquement une barre de navigation. Si des pages doivent être rajoutées ou enlevées dans le site, c'est dans ce fichier qu'il faudra écrire.
	\item \underline{Dossier \texttt{\_includes}} : Dans ce dossier sont définis des fichiers html qui pourront être inclus dans d'autres fichiers. Pour inclure un fichier du dossier \texttt{\_include/}, il faut utiliser la syntaxe de Liquid \texttt{\{\% include file.html \%\}}.
	\item \underline{Dossier \texttt{\_layouts}} : C'est ici que son définis les \textit{template}. Un template est un fichier HTML qui sert de base à d'autres fichiers HTML. Cela permet de factoriser le code commun dans un seul fichier, et évite de faire des copié collé sur toutes les pages au moindre changement. La balise \texttt{\{\{ content \}\}} dans le template définit l'emplacement où code supplémentaire se situera. On peut choisir le template que l'on va utiliser en écrivant dans le \textit{Front Matter} de la page :
\begin{minted}{yaml}
---
layout: nom_du_template
---
\end{minted}
	Le template \texttt{default.html} est le seul que nous utilisons et est appliqué sur toutes les pages. Il permet de charger la barre de navigation ainsi que les feuilles de styles css et les scripts js pour chaque page.
\end{itemize}

\subsection{Structure globale d'une page}
Chaque page du site commence par un \textit{Front Matter}. Plusieurs variables doivent être définies dans celui-ci.
\begin{enumerate}
	\item \underline{\texttt{layout:}} La variable \texttt{layout} doit être mise à \texttt{default} pour utiliser le template \texttt{default.html}.
	\item \underline{\texttt{title:}} Le nom de la page est défini ici. C'est ce nom qui sera affiché dans l'onglet du navigateur. On peut accéder au nom défini où l'on souhaite dans la page avec la syntaxe Liquid \texttt{\{\{ page.name \}\}}.
	\item \underline{Optionel : \texttt{custom\_css, js, third\_party\_js}} : permet de définir simplement des fichiers CSS ou JS à utiliser sans avoir à écrire les balises HTML. \\
	Exemple : On peut écrire le \textit{Front Matter} suivant :
	\begin{minted}{yaml}
---
custom_css:
- playground
- q
---
	\end{minted}
	Le template \texttt{default.html} générera les liens associés grâce à cette syntaxe :
	\begin{minted}{html}
{% for stylesheet in page.custom_css %}
   <link rel="stylesheet" href="assets/css/{{ stylesheet }}.css">
{% endfor %}
	\end{minted}
\end{enumerate}

\subsection{Articles et Markdown}

Le site possède 2 types de pages.
\begin{enumerate}
	\item Les pages dites \textit{articles} qui sont les pages de texte où sont expliqués les concepts abordés. Elles sont écrites avec la syntaxe Markdown (fichiers \texttt{.md}).
	\item Les pages \textit{animations}, qui possèdent des animations interactives permettant de visualiser les concepts. Elles sont écrites au format classique HTML et utilisent beaucoup de scripts JS pour réaliser les animations.
\end{enumerate}

\begin{figure}[h]
\centering
	\begin{subfigure}{0.4\textwidth}
		\centering
		\begin{minted}{yaml}
---
layout: default
title: Titre de la page
custom_css: article
js: article
third_party_js: mathjax
---
		\end{minted}
		\begin{minted}{html}
<div class="article">
# {{ page.title }}
{:.no_toc .title}
		\end{minted}
		\caption{Début d'un article}
	\end{subfigure}
	\begin{subfigure}{0.4\textwidth}
		\centering
		\begin{minted}{html}
	.
	.
	.

	</div>

	{% include toc.html %}
		\end{minted}
		\caption{Fin d'un article}
	\end{subfigure}
	\caption{Structure d'un article}
	\label{article}
\end{figure}

Jekyll permet de transformer une page écrite en format Markdown dans une syntaxe HTML lisible par un navigateur web. Le Markdown étant beaucoup plus lisible et simple à écrire que le HTML, cela facilite beaucoup la modification et la création de nouveaux articles. Le parseur Markdown utilisé ici est \texttt{kramdown} qui est le parseur principal utilisé par Jekyll.
Les symboles \texttt{\#} définissant les titres en Markdown sont alors convertis en balises \texttt{<h1>}, \texttt{<h2>}, les délimiteurs \texttt{**} par des balises \texttt{<strong>} ... Un guide succint mais très complet des équivalences Markdown/HTML est disponible à l'adresse \url{https://wprock.fr/blog/markdown-syntaxe/}. Il est possible de mixer HTML et Markdown dans le même fichier.

Le structure d'un article est toujours la même (voir Fig. \ref{article}) :
\begin{itemize}
	\item \underline{Début d'un article} : Le \textit{Front Matter} définit le template utilisé, le titre de la page, ainsi que les fichiers \texttt{.css} et \texttt{.js} qui seront utilisés. Une division "article" est ensuite définie avec la balise \texttt{<div>}, et le titre de la page est affichée. La syntaxe \texttt{\{:.no\_toc\}} permet de notifier que l'on ne souhaite pas ajouter cet élément à la table des matières. Globalement une syntaxe \texttt{\{:.nom\_de\_la\_classe\}} permet d'ajouter une classe CSS à l'élément lors de la conversion en HTML.
	\item \underline{Contenu de l'article} : On peut écrire ce que l'on veut, soit en Markdown, soit directement en HTML. Le contenu des sections doivent être entourées de la balise \texttt{<section>} si l'on souhaite que la table des matières puisse se colorer en fonction de la section actuelle.
	\item \underline{Fin de l'article} : La division "article" est refermée, puis la table des matières est incluse.
\end{itemize}


Une table des matières peut-être automatiquement générée par \texttt{kramdown}. Il suffit d'inclure le fichier \texttt{toc.html} à la fin de la page de l'article.

Tous les articles possèdent une feuille de style commune \texttt{assets/css/article.css} et utilisent tous le script \texttt{assets/js/article.js} ainsi que la librairie \texttt{MathJax}, permettant d'afficher des équations mathématiques écrites en LateX.

Il est très simple de créer de nouveaux articles en prenant exemple sur les articles déjà écrits.

\subsection{Pages d'animation}

Les pages d'animations permettent de visualiser les concepts abordés à travers des applications web interactives. Ces pages utilisent principalement des scripts JS. Une documentation des principales fonctions de chaque module de scripts, générée avec \textit{JSDoc} \cite{jsdoc} est disponible dans le dossier \texttt{jsdoc}.

\subsubsection{Sphère de Bloch}

La page interactive de la sphère de Bloch est disponible dans le fichier \texttt{qubitAnimation.html}. Cette page permet d'importer des états quantiques et de les visualiser sur la sphère, ainsi que d'observer les effets de rotation des différentes portes quantiques. Cette page est basée sur la librairie \texttt{grok-bloch} \cite{grokbloch}, à laquelle nous avons rajouté des fonctionnalitées supplémentaires :
\begin{enumerate}
	\item Formulaire pour rentrer un état quantique avec système de parsage de nombre complexe
	\item Visualisation des états quantiques en coordonnées sphériques $(\theta, \phi)$ et en coordonnées $(\ket{0}, \ket{1})$.
\end{enumerate}

La page HTML définit simplement le formulaire et les emplacements pour la visualisation des états, ainsi qu'une zone de mémo où les définitions matricielles des portes quantiques sont rappelées. La balise \texttt{<canvas>} définit la zone de dessin où est située la sphère de Bloch. Le script \texttt{assets/js/thirdparty/grok-bloch/main.js} se charge de dessiner dans ce \textit{canvas} pour créer la sphère de Bloch.

\subsubsection{Création de circuits quantiques}

La page interactive sur les circuits quantiques est disponible dans le fichier \texttt{portePlusieursQ-} \texttt{ubits.html}. Elle permet de créer des circuits en \textit{drag and drop} et d'observer l'évolution des probabilités du système. Cette page est basée sur la librairie \texttt{Q.js} \cite{q.js}, à laquelle nous avons également rajouté l'affichage de l'état quantique du système après chaque modification du circuit.

Les sources de \texttt{Q.js} sont disponibles dans le dossier \texttt{assets/js/thirdparty/Circuit/Q}. Les modifications ont été réalisées dans le fichier \texttt{Q-Circuit.js}. Si les sources ont besoin d'être modifiées de nouveau, il faut lancer le script \texttt{build.sh} après modification. Ce script met à jour les fichiers \texttt{q.js} et \texttt{q.css} finals utilisés dans le site.

La partie \texttt{<script>} dans le fichier \texttt{portePlusieursQubits.html} est pour l'affichage et la mise à jour du playground.

\subsubsection{Algorithme de Deutsch}

Le fichier \texttt{deutsch.html} contient la page exécutant l'algorithme de Deutsch. Il définit 4 boutons HTML permettant de choisir l'une des 4 fonctions pour l'oracle $U_f$. Lorsque l'utilisateur clique sur l'un des boutons, un script définit dans \texttt{assets/js/deutsch.js} se déclenche et fait apparaître le circuit de Deutsch.

Dans ce script, une classe \texttt{DeutschAlgorithm} a été créée. Elle encapsule toutes les données nécessaires pour la réalisation de l'algorithme : l'état courant du système, la fonction utilisée, l'étape actulle parmi les 4 étapes de l'algo ... Il utilise les fonctions fournis par la librairie \texttt{jsqubits} \cite{jsqubits} pour calculer les états quantiques. Nous avons largement modifié cette librairie afin qu'elle puisse afficher les états dans une syntaxe MathJax pour obtenir des équations lisibles. Nous avons également ajouté une fonctionnalité pour pouvoir convrtir l'état de la base $(\ket{0}, \ket{1})$ vers la base X $(\ket{+}, \ket{-})$, qui est plus simple à manier lors de l'utilisation de porte Hadamard.

La librairie \texttt{jsqubits} n'est pas très complexe à prendre en main. Il existe une documentation complète de l'API \url{https://davidbkemp.github.io/jsqubits/jsqubitsManual.html}. Seul les méthodes \texttt{setHadamardBasis(bool)} et \texttt{setMathjax(bool)} ont été rajoutées à l'interface. La première permet d'indiquer si les calculs ont lieu sur la base X où non, et la deuxième permet de générer les équations en syntaxe LateX parsable par MathJax.

La librairie possède plusieurs fichiers, notamment \texttt{Complex.js} et \texttt{QMath.js} où sont définis des classes et fonctions permettant de gérer les nombres complexes et les vecteurs quantiques. Le seul fichier intéressant si d'éventuelles modifications sont à réaliser est le fichier \texttt{QState.js}, qui définit la classe \texttt{QState} pour gérer l'état d'un système.

\subsubsection{Algorithme de Deutsch-Josza}

D'une manière similaire à l'algorithme de Deutsch, une page \texttt{deutschjosza.html} a été créée pour pouvoir exécuter l'algorithme de Deutsch-Josza, à l'aide du script \texttt{assets/js/deutschjosza.js}. Le fonctionnement est très similaire : le fichier HTML contient la page exécutant l’algorithme de Deutsch-Josza. Il définit au début un champ d'entrée pour saisir la valeur $n$ du nombre de paramètres de la fonction $f$. Trois boutons permettent ensuite de choisir l’un des 3 types de fonction pour $U_f$. Les différences entre ces deux algos :
\begin{enumerate}
    \item Cas à n bits : une fonction mathématique $f:\{0,1\}^{n}\rightarrow \{0,1\}$
    \item Entrée de n bits : qui est composé de (n-1) zéros et un 1
	\item Un générateur de fonction balancée aléatoire a été implémenté en plus afin d'éviter de devoir rentrer une fonction à la main.
\end{enumerate}

Une classe \texttt{DeutschJoszaFunction} a été également créée dans ce script. Les données encapsulées sont similaires avec les données de la classe \texttt{DeutschAlgorithm} décrite dans la section précédente. Il contient en plus deux variables spécialement définies pour \texttt{DeutschJoszaFunction} :
\begin{enumerate}
    \item \texttt{this.paramCount} : qui représente l'exponent $n$
	\item \texttt{this.stateString} : une chaîne de $n$ bits, comme : 00001
\end{enumerate}


\section{Pistes d'améliorations}

\begin{enumerate}
	\item Il peut être intéressant de rendre le site d'autant plus \textit{responsive} et de l'\textbf{adapter au support mobile}. Le site fonctionne pour smarthpone, mais la barre de navigation parait trop petite et certaines pages sont inutilisables.
	\item D'autres algorithmes quantiques pourraient être ajoutés, comme l'algorithme de Shor ou de Grover, avec des pages expliquant la transformée de Fourier quantique.
	\item Traduire la documentation JSDoc en 100\% anglais (elle est actuellement écrite en aprtie en anglais, à moitié en fraçais...)
	\item Créer une architecture plus modulaire en orienté objet : beaucoup de tronçons de code sont communs dans les scripts \texttt{deutschjosza.js} et \texttt{deutsch.js}. Il pourrait être intéressant de réfléchir à une vraie architecture pour le site (héritage de classes, méthodes privées / publiques ...), surtout si d'autres démonstrateurs viennent s'y ajouter. Cela permettrait de créer une interface plus maintenable et intuitive.
\end{enumerate}
